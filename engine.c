#include <pthread.h>
#include <string.h> // strtok, strcmp
#include <stdlib.h> // atoi
#include <time.h>   // nanosleep
#include <sched.h>  // setting priority.  

#include "threadville.h"

void engine_init(FILE * init_file){
    char buffer[BUFF_SIZE];
    char * operation_name; 
    
    printf("Engine init sequence\n");
    printf("\tSeeding time\n\tConstructing containers create_request vector\n");
    printf("\t                        alive_autos vector\n");
    printf("\t                        bridges vector\n");
    printf("\t                        repairs vector\n");
    srand(time(NULL));
    globals.exit            = 0;
    pthread_mutex_init(&globals.clock_mtx, NULL);
    globals.create_requests = vec_request_class.constructor();
    globals.alive_autos     = vec_autoPtr_class.constructor();
    globals.bridges         = vec_bridgePtr_class.constructor();
    globals.repairs         = vec_repair_class.constructor();

    while(fgets(buffer, BUFF_SIZE, init_file) != NULL){
        operation_name = strtok(buffer, "=");
        if(strcmp(operation_name, "set time") == 0){
            globals.tics_frequency = atoi(strtok(NULL, ","));
            globals.tics_per_toc = atoi(strtok(NULL, ";"));
            printf("\tGlobal variable tics_frequency set at %d\n", globals.tics_frequency);
            printf("\tGlobal variable tics_per_toc set at %d\n", globals.tics_per_toc);
        }
        else if(strcmp(operation_name, "load map") == 0){
            char * file_name = strtok(NULL, ";");
            FILE * f = open_check_file(file_name, "r");
            printf("\tLoading map from file %s\n", file_name);
            globals.map = load_map(f, &globals.size_x, &globals.size_y);
            fclose(f);
        } 
        else if(strcmp(operation_name, "load stops dict") == 0){
            char * file_name = strtok(NULL, ";");
            FILE * f = open_check_file(file_name, "r");
            printf("\tLoading stops from file %s\n", file_name);
            globals.stops_dict = load_stops_dict(f, globals.map);
            fclose(f);
        }
        else if(strcmp(operation_name, "load layer") == 0){
            char * file_name = strtok(NULL, ",");
            FILE * f = open_check_file(file_name, "r");
            int layer = atoi(strtok(NULL, ";"));
            printf("\tLoading layer %d from file %s\n", layer, file_name);
            load_layer(f, globals.map, layer);
            fclose(f);
        }
        else if(strcmp(operation_name, "load autos database") == 0){
            char * file_name = strtok(NULL, ";");
            FILE * f = open_check_file(file_name, "r");
            printf("\tLoading auto definitions from file %s\n", file_name);
            globals.autoDef_dict = load_autos_db(f);
            fclose(f);
        }
        else if(strcmp(operation_name, "new bridge") == 0){
            tile tmp;
            tile * entries[4];
            for(int i = 0; i < 4; ++i){
                tmp = (tile) {atoi(strtok(NULL, ",")), atoi(strtok(NULL, ",")), atoi(strtok(NULL, ";"))};
                entries[i] = globals.map->begin[globals.map->index(globals.map, &tmp, cmp_tiles)];
            }
            bridge * b = bridge_class.constructor(entries[0], entries[1], entries[2], entries[3], &globals.clock_cond, &globals.clock_mtx);
            globals.bridges->append(globals.bridges, b);
        }
        else if(strcmp(operation_name, "new auto") == 0){
            char * auto_def_name = strtok(NULL, ";");
            int index = globals.autoDef_dict->index(globals.autoDef_dict, auto_def_name);
            request requested;
            requested.type = index;
            requested.stops = NULL;
            globals.create_requests->append(globals.create_requests, requested);
            printf("\tNew car requested: %s, at index %d\n", auto_def_name, index);
        }
    }
}

int engine_main_loop(){
    automobile * dead_auto;
    automobile * new_auto;
    dict_charPtr_autoDef_entry new_autoDef;
    pthread_attr_t auto_attr_thread;
    struct sched_param param;
    struct timespec sleep_time = {0, 1e9/globals.tics_frequency};
    int zombies = 1;
    pthread_t repairs;
    pthread_create(&repairs, NULL, createRepairs, NULL);

    printf("Engine main loop\n");

    printf("\tWaking up %ld bridges\n", globals.bridges->length);
    for(vec_bridgePtr_itr b = globals.bridges->begin; b != globals.bridges->end; ++b)
        pthread_create(&((*b)->tid), NULL, (*b)->start, *b);

    while(!globals.exit) {
        /* Broadcast to conditional wait */
        pthread_mutex_lock(&globals.clock_mtx);
        pthread_cond_broadcast(&globals.clock_cond);
        pthread_mutex_unlock(&globals.clock_mtx);
        nanosleep(&sleep_time, NULL);

        /* Iterates over alive_autos vector searching for dead autos. 
           Zombie autos (not alive, but not dead) are ignored until 
           auto set itself as dead. 
           Dead autos are destroyed (free memory) and poped out. 
        */
        globals.alive_autos->protect(globals.alive_autos);
        for(int a = 0; a < globals.alive_autos->length; ++a)
            if(globals.alive_autos->begin[a]->dead){
                automobile_class.destroy(globals.alive_autos->begin[a]);
                globals.alive_autos->pop(globals.alive_autos, a--);
            } 

        /* Honors create request */
        globals.create_requests->protect(globals.create_requests);
        while(globals.create_requests->length != 0){
            request requested = globals.create_requests->pop(globals.create_requests, -1);
            new_autoDef = globals.autoDef_dict->begin[requested.type];
            new_auto = automobile_class.constructor(new_autoDef.value, globals.stops_dict, requested.stops, &globals.tics_per_toc, &globals.clock_cond, &globals.clock_mtx);
            globals.alive_autos->append(globals.alive_autos, new_auto);

            pthread_attr_init (&auto_attr_thread);
            pthread_attr_getschedparam (&auto_attr_thread, &param);
            param.sched_priority = new_auto->priority;
            pthread_attr_setschedparam (&auto_attr_thread, &param);

            pthread_create (&new_auto->tid, &auto_attr_thread, new_auto->start, new_auto);
            printf("\tNew car created from definition %s\n", new_autoDef.key);
        }
        globals.create_requests->release(globals.create_requests);
        globals.alive_autos->release(globals.alive_autos);

    }

    printf("\tExit Signal received\n\tSignaling kill to all autos and bridges\n");
    for(vec_autoPtr_itr a = globals.alive_autos->begin; a != globals.alive_autos->end; ++a)
        automobile_class.kill_signal(*a);
    for(vec_bridgePtr_itr b = globals.bridges->begin; b != globals.bridges->end; ++b)
        bridge_class.kill_signal(*b);


    sleep_time.tv_nsec = 1e6 ; // make a tic every millisecond
    printf("\tGenerating false clock, so everybody can die peacefully\n");
    while(zombies){
        // generate a tic
        pthread_mutex_lock(&globals.clock_mtx);
        pthread_cond_broadcast(&globals.clock_cond);
        pthread_mutex_unlock(&globals.clock_mtx);
        nanosleep(&sleep_time, NULL);

        zombies = 0;
        for(vec_autoPtr_itr a = globals.alive_autos->begin; a != globals.alive_autos->end; ++a)
            if(!((*a)->dead))
                zombies++;
        for(vec_bridgePtr_itr b = globals.bridges->begin; b != globals.bridges->end; ++b)
            if(!((*b)->dead))
                zombies++;
    }
    printf("\tEverybody is dead...\n");
    printf("\tLeaving engine main loop\n");
    return 0;
}

int engine_end(){

    printf("Engine end sequence\n");
    printf("\tDestroying autos, alive_autos, bridges and create_requests\n");
    for(vec_autoPtr_itr a = globals.alive_autos->begin; a != globals.alive_autos->end; ++a)
        automobile_class.destroy(*a);
    globals.alive_autos->destroy(globals.alive_autos);
    globals.bridges->destroy(globals.bridges);
    globals.create_requests->destroy(globals.create_requests);

    printf("\tDestroying tiles, unloading map and stops dictionary\n");
    for(dict_charPtr_tilePtr_itr i = globals.stops_dict->begin; i != globals.stops_dict->end; ++i)
        free(i->key);
    globals.stops_dict->destroy(globals.stops_dict);
    for(vec_tilePtr_itr t = globals.map->begin; t != globals.map->end; ++t)
        (*t)->destroy(*t);
    globals.map->destroy(globals.map);

    printf("\tDestroying auto definitions and repairs vector\n");
    for(dict_charPtr_autoDef_itr d = globals.autoDef_dict->begin; d != globals.autoDef_dict->end; ++d){
        free(d->value.stops);
        free(d->key);
    }
    globals.autoDef_dict->destroy(globals.autoDef_dict);
    globals.repairs->destroy(globals.repairs);

    printf("\tThis is all folks!\n");
    return 0;
}

/*
    This is a different thread because in this case where the the process has 
    to wait at least 30 secs to create another reparation it's easier to manage 
    the sleep time by seconds, rather than by nsecs. If we add this within the while 
    of main_loop we'll do too many unnecessary things each nsec 
*/
void * createRepairs() {

    int secs_to_sleep = 30;
    int min_secs_to_repair = 1;
    int max_secs_to_repair = 3;
    int road_tiles_count = 0;
    int is_valid_reparation = 0;
    struct timespec sleep_time;
    vec_tilePtr *street_map = vec_tilePtr_class.constructor();

    /*
        Creation of a vector just with the road tiles, 
        this code will take a couple of nsecs at the begining of 
        the program, but it's necessary otherwise the action of 
        create a repair will take a lot of time (nsecs) on each iteration
    */

    for(vec_tilePtr_itr t=globals.map->begin; t != globals.map->end; ++t) {
        if((*t)->name != NULL && 
            strcmp((*t)->name, "Z") != 0 && 
            strcmp((*t)->name, "Y") != 0 && 
            strcmp((*t)->name, "X") != 0)
        {
            street_map->append(street_map, (*t));
        }
    }

    road_tiles_count = street_map->length;

    while(!globals.exit) {

        repair r;
        is_valid_reparation = 0;
        sleep_time.tv_sec = get_random_number(secs_to_sleep, secs_to_sleep);
        nanosleep(&sleep_time, NULL);

        r.remaining_tics = get_random_number(min_secs_to_repair,max_secs_to_repair) * 5;

        /*
            This While checks if there is already the tile under reparation, 
            even when there is going to be just one reparation at the time, 
            it's better to have this validation and prevent possible inconsistencies
        */
        while(is_valid_reparation <= 0){
            is_valid_reparation = 0;
            r.t = street_map->begin[get_random_number(0,road_tiles_count-1)]; 
            for(vec_repair_itr rep=globals.repairs->begin; rep != globals.repairs->end; ++rep) {
                if((*rep).t->index == r.t->index){
                    printf("This tile is already under reparations > %s", (*rep).t->name);
                    rep = globals.repairs->end;
                    is_valid_reparation = -1;
                }
            }
            if(is_valid_reparation == 0) {
                is_valid_reparation++;
            }
        }
        
        //Init reparation
        r.t->on_service = 0;
        globals.repairs->append(globals.repairs,r);
        printf("\tStarted reparation at tile - %s - It'll take %i seconds long to finish.\n", r.t->name, r.remaining_tics);
        sleep_time.tv_sec = r.remaining_tics;
        nanosleep(&sleep_time, NULL);
        
        //Finish reparation
        r.t->on_service = 1;
        globals.repairs->pop(globals.repairs,-1);
        printf("\tFinished reparations at tile - %s.\n", r.t->name);
    }
    pthread_exit(NULL);
}

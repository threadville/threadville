#include <errno.h> // Reporting Busy mutex
#include <stdlib.h> // malloc

#include "tile.h"
#include "auto.h"

int cmp_tiles(tile * t1, tile * t2){
    return abs(t1->x - t2->x) + abs(t1->y - t2->y) + abs(t1->z - t2->z);
}

tile * tile_capture(tile * self, void * agent){
    if(!self->on_service)
        return NULL;
    if(pthread_mutex_trylock(&(self->mutex)) == EBUSY)
        return NULL;
    else{
        self->locked_by = agent;
        return self;
    }
}

void tile_release(tile * self){
    pthread_mutex_unlock(&(self->mutex));
    self->locked_by = NULL;
}

tile * tile_constructor(int x, int y, int z){
    tile * self = malloc(sizeof(tile));
    *self = tile_class;
    self -> x = x;
    self -> y = y;
    self -> z = z;
    return self;
}


int tile_weight(tile * self){
    if(self ->on_service) {
        // tile is on highway, we prefer that instead of normal tile.
        if (self->speed_up == 2)
            return 1;
        else
            return 3;
    }
    else {
        // tile is on service, we don't want to take this route.
        return 100;
    }
}



void tile_destroy(tile * self){
    free(self);
}

tile tile_class = { 0, 0, 0,                // x, y,z
                    -1,                      // index
                    NULL,                   // name
                    NULL, NULL, NULL, NULL, // links[4]
                    1,                      // speed_up
                    1,                      // On service
                    PTHREAD_MUTEX_INITIALIZER,
                    NULL,                   // Locked_by
                    tile_constructor,
                    tile_capture,
                    tile_release,
                    tile_destroy,
                    tile_weight
                  };

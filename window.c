//
// Created by kelvin on 5/28/17.
//

#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include "window.h"
#include "threadville.h"
#include "draw.h"

#define SCREEN_X 1192
#define PIXELS_X 1024
#define PIXELS_Y 512

GtkWidget *CreateCombobox ();
static void do_drawing(cairo_t *);
int pixel_to_int(double pixel);



static automobile* selected = NULL;
static int tile_length;
static GtkWidget * entry;
static GtkWidget *remain_stops_label;
static GtkWidget *next_stops_label;



static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr,
                              gpointer user_data)
{
    do_drawing(cr);

    return FALSE;
}

static gboolean close_application(GtkWidget *widget)
{
    globals.exit = 1;
    gtk_widget_destroy(widget);
    gtk_main_quit();
    return FALSE;
}

static gboolean create_car(GtkWidget *widget)
{
    char * type = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(widget));
    char * stops = NULL;
    if((int)gtk_entry_get_text_length(GTK_ENTRY(entry)) > 0){
        stops = string_alloc((char* ) gtk_entry_get_text(GTK_ENTRY(entry)));
        gtk_entry_set_text(GTK_ENTRY(entry),"\0");
    }

    create_car_type(type, stops);
    return FALSE;
}

static gboolean kill_car(GtkWidget *widget)
{
    if(selected != NULL && selected->alive)
    {
        selected->kill_signal(selected);
        gtk_label_set_text(GTK_LABEL(remain_stops_label),"Remaining:");
        gtk_label_set_text(GTK_LABEL(next_stops_label),"Next:");

    }
    return FALSE;
}

void create_car_type(char* type, char* stops)
{
    int index = globals.autoDef_dict->index(globals.autoDef_dict, type);
    request requestedCar;
    requestedCar.type = index;
    requestedCar.stops = stops;
    globals.create_requests->protect(globals.create_requests);
    globals.create_requests->append(globals.create_requests, requestedCar);
    globals.create_requests->release(globals.create_requests);

}

static double complement(double value)
{
    return (int)value == 1 ? 0 : 1.0f - value;
}



static void do_drawing(cairo_t *cr){
    int x, y; 

    // paint yellow background
    cairo_set_source_rgb(cr, 1, 1, 0);
    tile_length = PIXELS_X/globals.size_x < PIXELS_Y/globals.size_y ? PIXELS_X/globals.size_x : PIXELS_Y/globals.size_y;
    cairo_rectangle(cr, 0, 0, tile_length * globals.size_x, tile_length * globals.size_y);
    cairo_fill(cr);

    // paint streets
    for(vec_tilePtr_itr t=globals.map->begin; t != globals.map->end; ++t){
        x = (*t)->x * tile_length;
        y = (*t)->y * tile_length;
        if((*t)->z == 0){
            if(!(*t)->on_service){
                cairo_set_source_rgb(cr, 0.5,0,0);
                cairo_rectangle(cr, x, y, tile_length, tile_length);
                cairo_fill(cr);
            }

            else if((*t)->speed_up > 1)
                draw_tile((*t), cr, tile_length, x, y, 0.15,0.15,0.15);

            else
                draw_tile((*t), cr, tile_length, x, y,0.22,0.22,0.22);

        } else {
            cairo_set_source_rgb(cr, 0.41,0.41,0.41);
            cairo_rectangle(cr, x, y, tile_length, tile_length);
            cairo_fill(cr);
       }

       // print cars
       if((*t)->locked_by != NULL){
           automobile * car = (*t)->locked_by;
           cairo_set_source_rgb(cr, car->color[0],car->color[1],car->color[2]);
           cairo_rectangle(cr, x, y, tile_length*0.75, tile_length*0.75);
           cairo_fill(cr);
           if(selected == car)
           {
               cairo_set_source_rgb(cr,
                                    complement(car->color[0]),
                                    complement(car->color[1]),
                                    complement(car->color[2]));
               cairo_rectangle(cr, x, y, tile_length*0.75, tile_length*0.25);
               cairo_fill(cr);
           }

      }
    }

    char conds[4];
    cairo_select_font_face(cr, "Sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size(cr, 11.0);
    cairo_set_source_rgb(cr, 0, 0, 1);

    for(dict_charPtr_tilePtr_itr s = globals.stops_dict->begin; s != globals.stops_dict->end; ++s){
        for(int i = 0; i < 4; ++i)
            conds[i] = s->value->links[i] != NULL;
        if(!conds[3] && !conds[2] && !conds[1] && conds[0]){
            x = s->value->x * tile_length;
            if(strcmp(s->key, s->value->name) != 0) // is not the owner
                y = (s->value->y+2) * tile_length;
            else
                y = s->value->y * tile_length;
        } else if(!conds[3] && conds[2] && !conds[1] && !conds[0]){
            x = s->value->x * tile_length;
            if(strcmp(s->key, s->value->name) != 0) // is not the owner
                y = (s->value->y+2) * tile_length;
            else
                y = s->value->y * tile_length;
        } else if(!conds[3] && !conds[2] && conds[1] && !conds[0]){
            x = (s->value->x-1) * tile_length;
            y = (s->value->y+1) * tile_length;
        } else if(conds[3] && !conds[2] && !conds[1] && !conds[0]){
            x = (s->value->x+1) * tile_length;
            y = (s->value->y+1) * tile_length;
        } else {
            x = s->value->x * tile_length;
            y = s->value->y * tile_length;
        }
        cairo_move_to(cr, x, y);
        cairo_show_text(cr, s->key);
    }

    char remaining[20];
    char nextStop[20];

    memset(remaining, 0, sizeof(remaining));
    memset(nextStop, 0, sizeof(nextStop));
    // print selected car next stops and remaining stops
    if(selected != NULL && selected->alive){
        sprintf(remaining, "Remaining:%d",(int)(selected->stops->end - selected->next_stop));
        sprintf(nextStop, "Next:%s", selected->next_stop == selected->stops->end ? (*selected->stops->begin)->name : (*selected->next_stop)->name);
    } else {
        sprintf(remaining, "Remaining:");
        sprintf(nextStop, "Next:");
    }
    gtk_label_set_text(GTK_LABEL(remain_stops_label),remaining);
    gtk_label_set_text(GTK_LABEL(next_stops_label),nextStop);
}


GtkWidget *CreateCombobox ()
{
    char text[80];
    GtkWidget *combo = gtk_combo_box_text_new();
    /* --- Create the drop down portion of the combo --- */
    for(dict_charPtr_autoDef_itr c = globals.autoDef_dict->begin;
            c != globals.autoDef_dict->end; ++c){
        sprintf(text, "%s", c->key);
        gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), text);
    }
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo),0);

    return (combo);
}

gboolean button_press_event(GtkWidget * widget, GdkEventButton * event, gpointer data){

    if (event->button == GDK_BUTTON_PRIMARY){
        int x = pixel_to_int(event->x);
        int y = pixel_to_int(event->y);
        for(vec_autoPtr_itr a = globals.alive_autos->begin; a != globals.alive_autos->end; ++a){
            tile * t = *((*a)->current_tile);
            if(x == t->x && y == t->y){
                if(*a == selected)
                    selected = NULL;
                else
                    selected = (*a);
                break;
            }
        }
    }
    return TRUE;
}

int pixel_to_int(double pixel){
    return tile_length > 0 ? (int) pixel / tile_length : 0;
}

gboolean time_handler(GtkWidget * widget){
    gtk_widget_queue_draw(widget);
    return TRUE;
}

void * window_load(void * a){
    struct init_args * main_args = a;
     gtk_init(&(main_args->argc), &(main_args->argv));
     GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

     GtkWidget *hpaned = gtk_paned_new (GTK_ORIENTATION_HORIZONTAL);
     GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 4);
     GtkWidget *darea = gtk_drawing_area_new();
     GtkWidget *exit = gtk_button_new_with_mnemonic ("_Quit");
     GtkWidget *create = gtk_button_new_with_mnemonic ("_Create");
    GtkWidget *kill = gtk_button_new_with_mnemonic ("_Kill");
     GtkWidget *combo = CreateCombobox();
    GtkWidget *create_label = gtk_label_new("Create Vehicle");
    GtkWidget *set_stops_label = gtk_label_new("Stops[Optional]");
    GtkWidget *status_label = gtk_label_new("Selected Vehicle");
    remain_stops_label = gtk_label_new("Remaining:");
    next_stops_label = gtk_label_new("Next:");
     entry = gtk_entry_new();
     gtk_paned_pack1(GTK_PANED (hpaned), darea, TRUE, FALSE);
     gtk_widget_set_size_request (darea, PIXELS_X, -1);

     gtk_paned_pack2 (GTK_PANED (hpaned), vbox, FALSE, FALSE);
     gtk_widget_set_size_request (vbox, SCREEN_X - PIXELS_X, -1);

     gtk_container_add(GTK_CONTAINER(window), hpaned);

    gtk_box_pack_start(GTK_BOX(vbox), create_label, FALSE, FALSE, 0);
     gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), set_stops_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 0);
     gtk_box_pack_start(GTK_BOX(vbox), create, FALSE, FALSE, 5);

    gtk_box_pack_start(GTK_BOX(vbox), status_label, FALSE, FALSE, 15);
    gtk_box_pack_start(GTK_BOX(vbox), remain_stops_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), next_stops_label, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), kill, FALSE, FALSE, 5);
     gtk_box_pack_end(GTK_BOX(vbox), exit, FALSE, FALSE, 10);

     g_signal_connect (exit, "clicked",
                       G_CALLBACK (close_application), NULL);

     g_signal_connect_swapped (create, "clicked", G_CALLBACK (create_car), combo);
    g_signal_connect_swapped (kill, "clicked", G_CALLBACK (kill_car), combo);

     g_signal_connect (window, "delete_event", G_CALLBACK (close_application), window);

     g_signal_connect(G_OBJECT(darea), "draw",
                     G_CALLBACK(on_draw_event), NULL);

    g_signal_connect (darea, "button-press-event",
                      G_CALLBACK (button_press_event), NULL);

    gtk_widget_set_events (darea, gtk_widget_get_events (darea)
                                         | GDK_BUTTON_PRESS_MASK

                                         | GDK_POINTER_MOTION_MASK);

    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_window_set_default_size(GTK_WINDOW(window), PIXELS_X, PIXELS_Y);
    gtk_window_set_title(GTK_WINDOW(window), "Threadville");

    g_timeout_add(40, (GSourceFunc) time_handler, (gpointer) window);
    gtk_widget_show_all(window);
    time_handler(window);

    gtk_main();
    pthread_exit(NULL);
}



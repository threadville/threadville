#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tile.h"
#include "auto.h"

/* Prototypes */
void dijkstra(tile* source, tile*  destination, vec_tilePtr * route);

#endif

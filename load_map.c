#include <stdio.h>  // printf, fprintf, fgets
#include <string.h> // strtok, strcmp
#include <stdlib.h> // atoi

#include "threadville.h"

vec_tilePtr * load_map(FILE * map_file, int * size_x, int * size_y){

    char buffer[BUFF_SIZE];
    char * tile_str;
    int tile_value;
    int x = 0;
    int y = 0;
    vec_tilePtr_itr tile_itr;
    tile tmp;
    tile * new_tile;

    // Init map graph;
    vec_tilePtr * map = vec_tilePtr_class.constructor();

    // Fills map, a vector of tiles
    while(fgets(buffer, BUFF_SIZE, map_file) != NULL){
        tile_str = strtok(buffer, ",");
        while(tile_str != NULL){
            tile_value = atoi(tile_str);
            if(tile_value != 0){
                new_tile = tile_class.constructor(x, y, 0);
                if(tile_value & 16)
                    new_tile->speed_up = 2;
                map->append(map, new_tile);
                new_tile->index = map->length - 1;
            }
            tile_str = strtok(NULL, ",");
            x += 1;
        }
        *size_x = x;
        y += 1;
        x = 0;
    }
    *size_y = y;

    // Set links.
    fseek(map_file, 0, 0);
    tile_itr = map->begin;
    while(fgets(buffer, BUFF_SIZE, map_file) != NULL){
        tile_str = strtok(buffer, ",");
        while(tile_str != NULL){
            tile_value = atoi(tile_str);
            if(tile_value != 0){
                  // Can go left
                if(tile_value & 1){ 
                    tmp = (tile) {(*tile_itr)->x-1, (*tile_itr)->y, 0};
                    (*tile_itr)->links[0] = map->begin[map->index(map, &tmp, cmp_tiles)];
                } // Can go down
                if(tile_value & 2){ 
                    tmp = (tile) {(*tile_itr)->x, (*tile_itr)->y+1, 0};
                    (*tile_itr)->links[1] = map->begin[map->index(map, &tmp, cmp_tiles)];
                } // Can go right
                if(tile_value & 4){
                    tmp = (tile) {(*tile_itr)->x+1, (*tile_itr)->y, 0};
                    (*tile_itr)->links[2] = map->begin[map->index(map, &tmp, cmp_tiles)];
                } // Can go up
                if(tile_value & 8){
                    tmp = (tile) {(*tile_itr)->x, (*tile_itr)->y-1, 0};
                    (*tile_itr)->links[3] = map->begin[map->index(map, &tmp, cmp_tiles)];
                }
                tile_itr++;
            }
            tile_str = strtok(NULL, ",");
        }
    }

    return map;
}

void load_layer(FILE * layer_file, vec_tilePtr * map, int layer){
    char buffer[BUFF_SIZE];
    char * tile_str;
    int tile_value;
    int x = 0;
    int y = 0;
    int t;
    vec_tilePtr_itr tile_itr;
    tile tmp;
    tile * new_tile;

    // Fills map, a vector of tiles
    while(fgets(buffer, BUFF_SIZE, layer_file) != NULL){
        tile_str = strtok(buffer, ",");
        while(tile_str != NULL){
            tile_value = atoi(tile_str);
            if(tile_value != 0){
                if(tile_value & 16){ // map (layer 0) tile
                    tmp = (tile) {x, y, 0};
                    t = map->index(map, &tmp, cmp_tiles);
                    new_tile = *(map->begin + t);
                    new_tile->z = layer;
                } else {
                    new_tile = tile_class.constructor(x, y, layer);
                    map->append(map, new_tile);
                    new_tile->index = map->length - 1;
                }
            }
            tile_str = strtok(NULL, ",");
            x += 1;
        }
        y += 1;
        x = 0;
    }

//                    printf("this tile (%d, %d, %d) can go to (%d, %d, %d)\n", this_tile->x, this_tile->y, this_tile->z, this_tile->links[3]->x, this_tile->links[3]->y, this_tile->links[3]->z);
    // Set links.
    fseek(layer_file, 0, 0);
    x = y = 0;
    tile * this_tile;
    while(fgets(buffer, BUFF_SIZE, layer_file) != NULL){
        tile_str = strtok(buffer, ",");
        while(tile_str != NULL){
            tile_value = atoi(tile_str);
            if(tile_value != 0){
                  // Can go left
                if(tile_value & 1){ // x-1
                    tmp = (tile) {x, y, layer};
                    this_tile = *(map->begin + map->index(map, &tmp, cmp_tiles));
                    tmp.x -= 1;
                    if((t = map->index(map, &tmp, cmp_tiles)) == -1){ // no in this layer, go search in layer 0
                        tmp.z = 0;
                        t = map->index(map, &tmp, cmp_tiles);
                    }  
                    this_tile->links[0] = *(map->begin + t);
                } // Can go down
                if(tile_value & 2){ // y+1
                    tmp = (tile) {x, y, layer};
                    this_tile = *(map->begin + map->index(map, &tmp, cmp_tiles));
                    tmp.y += 1;
                    if((t = map->index(map, &tmp, cmp_tiles)) == -1){ // no in this layer, go search in layer 0
                        tmp.z = 0;
                        t = map->index(map, &tmp, cmp_tiles);
                    }  
                    this_tile->links[1] = *(map->begin + t);
                } // Can go right
                if(tile_value & 4){ // x+1
                    tmp = (tile) {x, y, layer};
                    this_tile = *(map->begin + map->index(map, &tmp, cmp_tiles));
                    tmp.x += 1;
                    if((t = map->index(map, &tmp, cmp_tiles)) == -1){ // no in this layer, go search in layer 0
                        tmp.z = 0;
                        t = map->index(map, &tmp, cmp_tiles);
                    }  
                    this_tile->links[2] = *(map->begin + t);
                } // Can go up
                if(tile_value & 8){ // y-1
                    tmp = (tile) {x, y, layer};
                    this_tile = *(map->begin + map->index(map, &tmp, cmp_tiles));
                    tmp.y -= 1;
                    if((t = map->index(map, &tmp, cmp_tiles)) == -1){ // no in this layer, go search in layer 0
                        tmp.z = 0;
                        t = map->index(map, &tmp, cmp_tiles);
                    }  
                    this_tile->links[3] = *(map->begin + t);
               }
            }
            tile_str = strtok(NULL, ",");
            x += 1;
        }
        y += 1;
        x = 0;
    }
}


dict_charPtr_tilePtr * load_stops_dict(FILE * stops_file, vec_tilePtr * map){

    char buffer[BUFF_SIZE];
    char * tile_str;
    tile tmp;
    tile *realTile;
    int x, y;

    // init stops dictionary
    dict_charPtr_tilePtr * stops_dict = dict_charPtr_tilePtr_class.constructor((int (*)(char *, char *)) strcmp); 

    while(fgets(buffer, BUFF_SIZE, stops_file) != NULL){
        char * name = strtok(buffer, ",");
        tile_str = string_alloc(name);
        x = atoi(strtok(NULL, ","));
        y = atoi(strtok(NULL, ","));
        tmp = (tile) {x, y};
        realTile = map->begin[map->index(map, &tmp, cmp_tiles)];
        if(realTile->name == NULL)
            realTile->name = tile_str;
        stops_dict->set(stops_dict, tile_str, realTile);
    }

    return stops_dict;
}

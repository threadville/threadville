#ifndef H_TILE
#define H_TILE

#include <stdio.h>
#include <pthread.h>


typedef struct tile tile;
struct tile{
    int x, y, z;
    int index;
    char* name;
    tile * links[4];
    int speed_up; 
    int on_service;
    pthread_mutex_t mutex;
    void * locked_by;
    tile * (*constructor) (int, int, int);
    tile * (*capture)(tile *, void *);
    void (*release)(tile *);
    void (*destroy)(tile *);
    int (*weight)(tile *);
};
extern tile tile_class;

int cmp_tiles(tile *, tile *);

#endif

#ifndef H_THREADVILLE
#define H_THREADVILLE

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "gdasc/gdasc_vector.h"
#include "gdasc/gdasc_dictionary.h"
#include "utils.h"
#include "tile.h"
#include "auto.h"
#include "bridge.h"
#include "window.h"

// Error codes
#define NO_CONF_FILE  1

typedef struct {
    int remaining_tics;
    tile * t;
} repair;

typedef struct{
    int type;
    char* stops;
}request;

/* Containers declaration */
VECTOR_DECLARE(request, vec_request)
VECTOR_DECLARE(repair, vec_repair)
VECTOR_DECLARE(bridge *, vec_bridgePtr);

struct globals{
    int exit;
    int size_x, size_y;
    int tics_frequency;
    int tics_per_toc;
    pthread_mutex_t clock_mtx;
    pthread_cond_t clock_cond;
    vec_tilePtr *map;
    dict_charPtr_tilePtr *stops_dict;
    dict_charPtr_autoDef *autoDef_dict;
    vec_request *create_requests;
    vec_autoPtr *alive_autos;
    vec_bridgePtr * bridges;
    vec_repair *repairs;
} globals;

/* Prototypes */
vec_tilePtr * load_map(FILE * map_file, int * x, int * y);
dict_charPtr_tilePtr * load_stops_dict(FILE * stops_file, vec_tilePtr * map);
void load_layer(FILE * layer_file, vec_tilePtr * map, int level);
void engine_init(FILE * init_file);
void * createRepairs();
int engine_main_loop();
int engine_end();

#endif

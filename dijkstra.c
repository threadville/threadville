#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include <stdio.h>
#include <values.h>
#include "threadville.h"
#include "minHeap.h"

void dijkstra(tile* source, tile*  destination, vec_tilePtr * route) {
    int size = globals.map->length;
    int distance[size];
    int parent[size][1];
    // minHeap represents set E
    struct MinHeap *minHeap = createMinHeap(size);

    struct MinHeapNode **array = malloc(size * sizeof(struct MinHeapNode*));
    // Initialize min heap with all vertices. dist value of all vertices
    for (int v = 0; v < size; ++v) {
        distance[v] = INT_MAX;
        minHeap->array[v] = newMinHeapNode(v, distance[v]);
        minHeap->pos[v] = v;
        parent[v][0] = -1;
        array[v] = minHeap->array[v];
    }

    // Make dist value of src vertex as 0 so that it is extracted first
    int src = source->index;
    distance[src] = 0;
    struct MinHeapNode* srcMinHeap = minHeap->array[src];
    srcMinHeap->dist = distance[src];
    minHeap->pos[src] = src;

    decreaseKey(minHeap, src, distance[src]);

    // Initially size of min heap is equal to V
    minHeap->size = size;

    // In the following loop, min heap contains all nodes
    // whose shortest distance is not yet finalized.
    while (!isEmpty(minHeap)) {
        // Extract the vertex with minimum distance value
        struct MinHeapNode * minHeapNode = extractMin(minHeap);
        int u = minHeapNode->v; // Store the extracted vertex number

        // Traverse through all adjacent vertices of u (the extracted
        // vertex) and update their distance values
        tile *node = globals.map->begin[u];

        for (int i = 0; i < 4; i++) {
            if (node->links[i] == NULL) continue;
            tile *actual = node->links[i];
            int v = actual->index;
            int parentNodeIndex = parent[node->index][0];
            tile* parentNode = parentNodeIndex != -1 ? globals.map->begin[parentNodeIndex] : NULL;
            //we need to skip if the actual tile has a link to its parent.
            if(parent[node->index][0] == v)
                continue;
            //if(node->z > 0 && parentNode != NULL &&
            //           parentNode->z == 0 &&
            //        parentNode->links[i] == NULL)
            //   continue;
            int weight = actual->weight(actual);
            // If shortest distance to v is not finalized yet, and distance to v
            // through u is less than its previously calculated distance
            if (isInMinHeap(minHeap, v) && distance[u] != INT_MAX &&
                weight + distance[u] < distance[v]) {
                distance[v] = distance[u] + weight;
                parent[v][0] = u;
                // update distance value in min heap also
                decreaseKey(minHeap, v, distance[v]);
            }
        }
        //free(minHeapNode);
    }

    tile* backtrack = destination;
    //route->append(route,destination);
    while(parent[backtrack->index][0] != -1){
        route->insert(route,backtrack,0);
        int parentIndex = parent[backtrack->index][0];
        backtrack = globals.map->begin[parentIndex];
    }

    for(int v=0;v<size;v++)
    {
        free(array[v]);
    }

    free(array);
    destroyMinHeap(minHeap);
}


#endif


#include "bridge.h"

/* Containers code */
VECTOR_GEN_CODE(automobile *, vec_autoPtr)


bridge * bridge_constructor(tile * entry1, tile * sensor1, tile * entry2, tile * sensor2, pthread_cond_t * clock_cond, pthread_mutex_t * clock_mtx){

    bridge * self = malloc(sizeof(bridge));
    *self = bridge_class;
    self->clock_cond = clock_cond;
    self->clock_mtx  = clock_mtx;
    self->entry1  = entry1;
    self->entry2  = entry2;
    self->sensor1 = sensor1;
    self->sensor2 = sensor2;
    self->autos_on_bridge = vec_autoPtr_class.constructor();
    return self;
}

void bridge_kill(bridge * self){
    self->alive = 0;
}

void bridge_destroy(bridge * self){
    self->autos_on_bridge->destroy(self->autos_on_bridge);
    free(self);
}

void * bridge_start(void * s){
    bridge * self = s;
    int index;
    tile * in_sensor, * out_sensor;

    while(self->alive){
        /* wait for clock tic */
        pthread_mutex_lock(self->clock_mtx);
        pthread_cond_wait(self->clock_cond, self->clock_mtx);
        pthread_mutex_unlock(self->clock_mtx);
       
        // Update autos currently in bridge
        if(self->entry1->locked_by != NULL)
            if(vec_autoPtr_class.count(self->autos_on_bridge, self->entry1->locked_by, cmp_autosPtr) == 0)
                vec_autoPtr_class.append(self->autos_on_bridge, self->entry1->locked_by);
        if(self->entry2->locked_by != NULL)
            if(vec_autoPtr_class.count(self->autos_on_bridge, self->entry2->locked_by, cmp_autosPtr) == 0)
                vec_autoPtr_class.append(self->autos_on_bridge, self->entry2->locked_by);
        if(self->sensor1->locked_by != NULL)
            if((index = vec_autoPtr_class.index(self->autos_on_bridge, self->sensor1->locked_by, cmp_autosPtr)) != -1)
                vec_autoPtr_class.pop(self->autos_on_bridge, index);
        if(self->sensor2->locked_by != NULL)
            if((index = vec_autoPtr_class.index(self->autos_on_bridge, self->sensor2->locked_by, cmp_autosPtr)) != -1)
                vec_autoPtr_class.pop(self->autos_on_bridge, index);

        // Outputs and next state
        switch(self->state){
            case BRIDGE_STATE_IDLE:
                self->entry1->on_service = 1;
                self->entry2->on_service = 1;
                if(self->entry1->locked_by != NULL)
                    self->state = BRIDGE_STATE_ACCESS1;
                else if(self->entry2->locked_by != NULL)
                    self->state = BRIDGE_STATE_ACCESS2;
                break;
            case BRIDGE_STATE_ACCESS1:
                self->entry1->on_service = 1;
                self->entry2->on_service = 0;
                if(self->autos_on_bridge->length == 0)
                    self->state = BRIDGE_STATE_IDLE;
                break;
            case BRIDGE_STATE_ACCESS2:
                self->entry1->on_service = 0;
                self->entry2->on_service = 1;
                if(self->autos_on_bridge->length == 0)
                    self->state = BRIDGE_STATE_IDLE;
               break;
        }
    }
    self->entry1->on_service = 1;
    self->entry2->on_service = 1;
    self->dead = 1;
    pthread_exit(NULL);
}

bridge bridge_class = {
    0,                 // tid
    NULL,              // clock_cond
    NULL,              // clock_mtx
    1,                 // alive
    0,                 // dead
    bridge_constructor,
    bridge_start, 
    bridge_kill,
    bridge_destroy, 
    BRIDGE_STATE_IDLE, // state
    NULL,              // entry1
    NULL,              // sensor1
    NULL,              // entry2
    NULL,              // sensor2
    NULL               // autos_on_bridge
};

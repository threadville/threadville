#ifndef H_BRIDGE
#define H_BRIDGE

#include <pthread.h>

#include "auto.h"

/* Containers Declaration */  
VECTOR_DECLARE(automobile *, vec_autoPtr)

#define BRIDGE_STATE_IDLE    1
#define BRIDGE_STATE_ACCESS1 2
#define BRIDGE_STATE_ACCESS2 3


typedef struct bridge bridge;
struct bridge {
    pthread_t tid;
    pthread_cond_t * clock_cond;
    pthread_mutex_t * clock_mtx;
    int alive;
    int dead;
    bridge * (*constructor) (tile *, tile *, tile *, tile *, pthread_cond_t *, pthread_mutex_t *);
    void *   (*start)       (void *);
    void     (*kill_signal) (bridge *);
    void     (*destroy)     (bridge *);
    int state;
    tile * entry1;
    tile * sensor1;
    tile * entry2;
    tile * sensor2;
    vec_autoPtr * autos_on_bridge;
};

extern bridge bridge_class;

#endif

#include <gtk/gtk.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>

//the global pixmap that will serve as our buffer


static GdkPixmap *pixmap = NULL;
#define TV_WIDTH 1250
#define TV_HEIGHT 700


cairo_surface_t *image;

// UI Interface begin

typedef struct  {
    int x;
    int y;
    float angle;
    int width;
    int height;
    float r;
    float g;
    float b;
} _ui_vehicle;


char **ui_buses;    //lista de buses que pueden activarse o desactivarse ej.: ui_buses = (char *[]) {"rojo","azul"}; La lista debe asinarse en memoria e inicializarse.
char **ui_auto_colors;    //lista de colores para los carros que seran generados. La lista debe asignarse en memoria e inicializarse.
char **ui_auto_destinations;     // lista de destinos para los carras que seran generados. La lista debe asignarse en memoria y inicializarse.
int ui_buses_count;    //dimension de la lista de buses, para el ejemplo de **ui_buses seria 2. 
int ui_auto_colors_count;   //dimension de la lista de colores para los carros que seran generados
int ui_auto_destinations_count;  //dimension de la lista de destinos para para los carros que seran generados
char ui_auto_color[50]="";    // guarda el valor introducido en el combo box de colores para los carros. Inicializarlo al primer valor que aparece en el combo box al inicio de la aplicacion (ej. el primer elemento de la lista de colores)
char ui_auto_destination[50]="";    // guarda el valor introducido en el combo box de destinos para los carros. Iniciazarlo al primer valor que parece en el combo box al inicio de la aplicacion (ej. el primero elemento de la lista de destinos)

void (*callb_start_simulation) (void) =  0; // Inicializar a la runtia de inicio de la simulacion
void (*callb_activate_bus) (const char *) = 0;   // Inicializar a la runtina que activa el bus. Recibe una cadena de caracteres de la lista de buses que pueden activarse o desactivarse.
void (*callb_deactivate_bus) (const char *) =  0;    // Inicializar a la runitna que desactiva el bus. Recibe una cadena de caracteres de la lista de buses que que pueden activarse o desactivarse. 
void (*callb_generate_auto) (void) = 0;    // Inicializar a la rutina que se encarga de generar un auto con caracteristicas aleatorias.
void (*callb_generate_ambulance) (void) =  0;   //Inicializar a la rutina que se encarga de generar una ambulancia.
void (*callb_generate_auto_with) (const char *) = 0; //Inicializar a la rutina que se encarga de generar el automovil con las siguientes caracteristicas: color y destino. Recibe como parametro una cadena de caracteres que contiene dos palabras separadas por un espacio. La primera palabra es el color y la segunda es el destino.

_ui_vehicle *ui_vehicles;  //lista de vehiculos que se utiliza para deibujar sobre threadville. La lista debe asignarse a memoria e inicializarse
int ui_vehicles_count;   //dimension de vehiculos qe de la lista de vehiculos que debe dibujarse.

void (*callb_get_updated_ui_vehicles) (void) = 0;  //Inicializar a la rutina que se encarga de actualizar la lista de vehiculos que debe dibujarse. Se espera que esta rutine actualice las propiedades y dimension (cantidad) de los vehiculos que se van a dibujar. Esta runtina se llama cada 33 milisegundos.

// UI Interface end

// funciones de muestra para demostracion. Para la demostracion se utilizan al asignarlas a los callbacs (eventos) del UI

void get_updated_ui_vehicles(void)
{
    static int i = 0;
    ++i; i = i % TV_WIDTH;   
    ui_vehicles[0].r = 0;
    ui_vehicles[0].g = 0;
    ui_vehicles[0].b = 1;
    ui_vehicles[0].angle=0;
    ui_vehicles[0].x = i;
    ui_vehicles[0].y = 0; 
    ui_vehicles[0].width = 20;
    ui_vehicles[0].height = 10;           
    ui_vehicles[1].r = 0;
    ui_vehicles[1].g = 0;
    ui_vehicles[1].b = 1;
    ui_vehicles[1].angle=2*3.1416*i/TV_WIDTH;
    ui_vehicles[1].x = i;
    ui_vehicles[1].y = 50; 
    ui_vehicles[1].width = 20;
    ui_vehicles[1].height = 10;           
}

void start_simulation(void)
{
    printf("Starting simulation\n");
}

void activate_bus(const char* option)
{
    printf("Activate bus: %s\n", option);
}

void deactivate_bus(const char* option)
{
    printf("Deactivate bus: %s\n", option);
}

void generate_auto(void)
{
    printf("Generate auto\n");
}

void generate_ambulance(void)
{
    printf("Generate ambulance\n");
}

void generate_auto_with(const char* option)
{
    printf("Generate auto with: %s\n", option); 
}


_ui_vehicle _vehicles[30];
char *_buses[30];
char *_auto_colors[15];
char *_auto_destinations[100];
char __buses[30][50];
char __auto_colors[15][50];
char __auto_destinations[100][50];

void init_demo_interface(void)
{
    ui_vehicles = &_vehicles[0];
    ui_vehicles[0].x = 0;
    ui_vehicles[0].y = 0;
    ui_vehicles[0].width = 20;
    ui_vehicles[0].height = 10;
    ui_vehicles[0].angle = 0;
    ui_vehicles[0].r = 0;
    ui_vehicles[0].g = 0;
    ui_vehicles[0].b = 1;    

    ui_vehicles[1].x = 0;
    ui_vehicles[1].y = 0;
    ui_vehicles[1].width = 20;
    ui_vehicles[1].height = 10;
    ui_vehicles[1].angle = 0;
    ui_vehicles[1].r = 0;
    ui_vehicles[1].g = 0;
    ui_vehicles[1].b = 1;    
    ui_vehicles_count = 2;
    callb_get_updated_ui_vehicles = &get_updated_ui_vehicles;

    ui_buses = &_buses[0];
    _buses[0] = &(__buses[0][0]);
    _buses[1] = &(__buses[1][0]);
    strcpy(ui_buses[0],"rojo");
    ui_buses[1] = _buses[1];
    strcpy(ui_buses[1],"azul");
    ui_buses_count = 2;
//    printf("bus 0 %s | bus 1 %s\n",ui_buses[0],ui_buses[1]);

    ui_auto_colors = &_auto_colors[0];
    _auto_colors[0] = &(__auto_colors[0][0]);
    strcpy(ui_auto_colors[0],"rojo");
    strcpy(ui_auto_color,ui_auto_colors[0]);
    ui_auto_colors_count=1;

    ui_auto_destinations = &_auto_destinations[0];
    _auto_destinations[0] = &(__auto_destinations[0][0]);
    strcpy(ui_auto_destinations[0],"A1");
    strcpy(ui_auto_destination,ui_auto_destinations[0]);
    ui_auto_destinations_count=1;
  
    callb_start_simulation = &start_simulation;
    callb_activate_bus = &activate_bus;
    callb_deactivate_bus = &deactivate_bus;
    callb_generate_auto = &generate_auto;
    callb_generate_ambulance = &generate_ambulance;
    callb_generate_auto_with = &generate_auto_with;

}
// fin funciones de muestra para demostracion.


// UI variables

char const *i_s_simulation = "s_simulation";
char const *i_a_bus = "a_bus";
char const *i_d_bus = "d_bus";
char const *i_g_auto = "g_auto";
char const *i_g_ambulance = "g_ambulance";
char const *i_g_auto_with = "g_auto_with";
char const *i_r_command = "r_command";
char ui_instruction[50];
char ui_parameters[250];
char ui_command[300];
char ui_message[300];
GtkWidget *label_message;

// Despachador de comandos

void dispatch_command(const char *command)
{
   sscanf(command, "%s %[^\n]",ui_instruction,ui_parameters);
   //printf(" command: %s | instruction: %s | parameters: %s\n ",command,ui_instruction,ui_parameters);
   if (strcmp(ui_instruction,i_s_simulation) == 0)
   {
       if (callb_start_simulation) callb_start_simulation();
       return;
   }
   if (strcmp(ui_instruction,i_a_bus) == 0)
   {
       if (callb_activate_bus) callb_activate_bus(ui_parameters);
       return;
   }
   if (strcmp(ui_instruction,i_d_bus) == 0)
   {
       if (callb_deactivate_bus) callb_deactivate_bus(ui_parameters);
       return;
   }
   if (strcmp(ui_instruction,i_g_auto) == 0)
   {
       if (callb_generate_auto) callb_generate_auto();
       return;
   }
   if (strcmp(ui_instruction,i_g_ambulance) == 0)
   {
       if (callb_generate_ambulance) callb_generate_ambulance();
       return;
   }
   if (strcmp(ui_instruction,i_g_auto_with) == 0)
   {
       if (callb_generate_auto_with) callb_generate_auto_with(ui_parameters);
       return;
   }
   if (strcmp(ui_instruction,i_r_command) == 0)
   {
       dispatch_command(ui_parameters);      
       return;
   }
   strcpy(ui_message,"Failed: ");
   strcat(ui_message,command);
   gtk_label_set_text(GTK_LABEL(label_message),ui_message );
  
}


//Events

void ui_start_simulation_event( GtkWidget *widget, gpointer   data )
{
   strcpy(ui_command,i_s_simulation);
   dispatch_command(ui_command);
}

void ui_activate_bus_event( GtkWidget *widget, gpointer data)
{
    char bus[50];
    const char *option = gtk_entry_get_text (GTK_ENTRY(widget));
    strcpy(bus,option);
    strcpy(ui_command, i_a_bus);
    strcat(ui_command, " ");
    strcat(ui_command, bus);
    dispatch_command(ui_command);  
}

void ui_deactivate_bus_event( GtkWidget *widget, gpointer data)
{
    char bus[50];
    const char *option = gtk_entry_get_text (GTK_ENTRY(widget));
    strcpy(bus,option);
    strcpy(ui_command, i_d_bus);
    strcat(ui_command, " ");
    strcat(ui_command, bus);
    dispatch_command(ui_command);  
}

void ui_generate_auto_event( GtkWidget *widget, gpointer   data )
{
   strcpy(ui_command,i_g_auto);
   dispatch_command(ui_command);
}

void ui_generate_ambulance_event( GtkWidget *widget, gpointer   data )
{
   strcpy(ui_command,i_g_ambulance);
   dispatch_command(ui_command);
}

void ui_auto_colors_event( GtkWidget *widget, gpointer data)
{
    const char *option = gtk_entry_get_text (GTK_ENTRY(widget));
    strcpy(ui_auto_color,option);
}

void ui_auto_destinations_event( GtkWidget *widget, gpointer data)
{
    const char *option = gtk_entry_get_text (GTK_ENTRY(widget));
    strcpy(ui_auto_destination,option);
}

void ui_generate_auto_with_event( GtkWidget *widget, gpointer   data )
{
   strcpy(ui_command, i_g_auto_with);
   strcat(ui_command, " ");
   strcat(ui_command, ui_auto_color);
   strcat(ui_command, " ");
   strcat(ui_command, ui_auto_destination);
   printf("command for generating auto with: %s\n",ui_command);
   dispatch_command(ui_command);
}

void ui_input_command_event( GtkWidget *widget, gpointer data)
{
    const char *command = gtk_entry_get_text (GTK_ENTRY(widget));
    strcpy(ui_command,command);   
}

void ui_run_command_event( GtkWidget *widget, gpointer   data )
{
   
   dispatch_command(ui_command);
}


gboolean on_window_configure_event(GtkWidget * da, GdkEventConfigure * event, gpointer user_data){
    static int oldw = 0;
    static int oldh = 0;
    //make our selves a properly sized pixmap if our window has been resized
    if (oldw != event->width || oldh != event->height){
        //create our new pixmap with the correct size.
        //GdkPixmap *tmppixmap = gdk_pixmap_new(da->window, event->width,  event->height, -1);
        GdkPixmap *tmppixmap = gdk_pixmap_new(da->window, TV_WIDTH,  TV_HEIGHT, -1);
        //copy the contents of the old pixmap to the new pixmap.  This keeps ugly uninitialized
        //pixmaps from being painted upon resize
        int minw = oldw, minh = oldh;
        if( event->width < minw ){ minw =  event->width; }
        if( event->height < minh ){ minh =  event->height; }
        //gdk_draw_drawable(tmppixmap, da->style->fg_gc[GTK_WIDGET_STATE(da)], pixmap, 0, 0, 0, 0, minw, minh);
        gdk_draw_drawable(tmppixmap, da->style->fg_gc[GTK_WIDGET_STATE(da)], pixmap, 0, 0, 0, 0, TV_WIDTH, TV_HEIGHT);
        //we're done with our old pixmap, so we can get rid of it and replace it with our properly-sized one.
        g_object_unref(pixmap);
        pixmap = tmppixmap;
    }
    oldw = event->width;
    oldh = event->height;
    return TRUE;
}

gboolean on_window_expose_event(GtkWidget * da, GdkEventExpose * event, gpointer user_data){
    gdk_draw_drawable(da->window,
        da->style->fg_gc[GTK_WIDGET_STATE(da)], pixmap,
        // Only copy the area that was exposed.
        event->area.x, event->area.y,
        event->area.x, event->area.y,
        TV_WIDTH, TV_HEIGHT);
        //event->area.width, event->area.height);
//      return FALSE;
    return TRUE;
}


// Fin de eventos

GtkWidget *CreateCombobox (char **list, int count)
{
    GList *cbitems = NULL;
    GtkWidget *combo;

    /* 
     * --- Create a list of the items first
     */
    int i;
    for (i=0; i < count ; ++i)
    {
        cbitems = g_list_append (cbitems, list[i]);
    }

    /* --- Make a combo box. --- */
    combo = gtk_combo_new ();

    /* --- Create the drop down portion of the combo --- */
    gtk_combo_set_popdown_strings (GTK_COMBO(combo), cbitems);

    /* --- Default the text in the field to a value --- */
    gtk_entry_set_text (GTK_ENTRY (GTK_COMBO(combo)->entry), list[0]);

    /* --- Make the edit portion non-editable.  They can pick a 
     *     value from the drop down, they just can't end up with
     *     a value that's not in the drop down.
     */
    gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);

    /* --- Make it visible --- */
    gtk_widget_show (combo);
    
    return (combo);
}


static int currently_drawing = 0;
//do_draw will be executed in a separate thread whenever we would like to update
//our animation
void *do_draw(void *ptr){

    //prepare to trap our SIGALRM so we can draw when we recieve it!
    siginfo_t info;
    sigset_t sigset;

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGALRM);

    while(1){
        //wait for our SIGALRM.  Upon receipt, draw our stuff.  Then, do it again!
        while (sigwaitinfo(&sigset, &info) > 0) {

            currently_drawing = 1;

            int width, height;
            gdk_threads_enter();
            gdk_drawable_get_size(pixmap, &width, &height);
            gdk_threads_leave();

            //create a gtk-independant surface to draw on
            cairo_surface_t *cst = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);
            cairo_t *cr = cairo_create(cst);

            //do some time-consuming drawing

            cairo_set_source_surface(cr, image,0,0);
            cairo_paint(cr);
            
            if (callb_get_updated_ui_vehicles) callb_get_updated_ui_vehicles();

            int k;
            for ( k=0 ; k<ui_vehicles_count ; ++k)
            {
                float r,g,b;
                float angle;
                int x,y,w,h;
                r = ui_vehicles[k].r;
                g = ui_vehicles[k].g;
                b = ui_vehicles[k].b;
                angle = ui_vehicles[k].angle;
                x = ui_vehicles[k].x;
                y = ui_vehicles[k].y;
                w = ui_vehicles[k].width;
                h = ui_vehicles[k].height;
                cairo_set_source_rgb (cr,r,g,b);
                cairo_save(cr);
                cairo_translate(cr, x,y);
                cairo_rotate(cr, angle);
                cairo_rectangle(cr, 0, 0, w, h);    
                cairo_fill(cr);        
                cairo_restore(cr);           
             }

           
            cairo_destroy(cr);


            //When dealing with gdkPixmap's, we need to make sure not to
            //access them from outside gtk_main().
            gdk_threads_enter();

            cairo_t *cr_pixmap = gdk_cairo_create(pixmap);
            cairo_set_source_surface (cr_pixmap, cst, 0, 0);
            cairo_paint(cr_pixmap);
            cairo_destroy(cr_pixmap);

            gdk_threads_leave();

            cairo_surface_destroy(cst);

            currently_drawing = 0;

        }
    }
}

gboolean timer_exe(GtkWidget * window){
    static int first_time = 1;
    //use a safe function to get the value of currently_drawing so
    //we don't run into the usual multithreading issues
    int drawing_status = g_atomic_int_get(&currently_drawing);

    //if this is the first time, create the drawing thread
    static pthread_t thread_info;
    if(first_time == 1){
        int  iret;
        iret = pthread_create( &thread_info, NULL, do_draw, NULL);
    }

    //if we are not currently drawing anything, send a SIGALRM signal
    //to our thread and tell it to update our pixmap
    if(drawing_status == 0){
        pthread_kill(thread_info, SIGALRM);
    }

    //tell our window it is time to draw our animation.
    int width, height;
    gdk_drawable_get_size(pixmap, &width, &height);
    gtk_widget_queue_draw_area(window, 0, 0, width, height);


    first_time = 0;
    return TRUE;

}



int main (int argc, char *argv[]){
    // Load image
    image = cairo_image_surface_create_from_png("threadville.png");

    init_demo_interface(); // comment out or remove after integratio.
   
    //Block SIGALRM in the main thread
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGALRM);
    pthread_sigmask(SIG_BLOCK, &sigset, NULL);

    //we need to initialize all these functions so that gtk knows
    //to be thread-aware
    if (!g_thread_supported ()){ g_thread_init(NULL); }
    gdk_threads_init();
    gdk_threads_enter();

    gtk_init(&argc, &argv);

    GtkWidget *window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    GtkWidget *box1 = gtk_vbox_new (FALSE, 0);
    GtkWidget *box2 = gtk_hbox_new (FALSE, 0);
    GtkWidget *box3 = gtk_hbox_new (FALSE, 0);
    GtkWidget *box4 = gtk_hbox_new (FALSE, 0);
    GtkWidget *box5 = gtk_hbox_new (FALSE, 0);


    GtkWidget *darea = gtk_drawing_area_new();
    GtkWidget *button = gtk_button_new_with_label ("Start Simulation");
    GtkWidget *label_activate_bus = gtk_label_new("Activate Bus");
    GtkWidget *combo_box_activate_bus;
    GtkWidget *label_deactivate_bus = gtk_label_new("Deactivate Bus");
    GtkWidget *combo_box_deactivate_bus;     
    GtkWidget *button_g_auto = gtk_button_new_with_label ("Generate auto");
    GtkWidget *button_g_ambulance = gtk_button_new_with_label ("Generate ambulance");
    GtkWidget *button_g_auto_with = gtk_button_new_with_label ("Generate auto with");
    GtkWidget *combo_box_auto_colors;
    GtkWidget *combo_box_auto_destinations;
    GtkWidget *txt_input_command = gtk_entry_new( ); 
    GtkWidget *button_execute_command = gtk_button_new_with_label("Execute command");
    label_message = gtk_label_new("");   

    combo_box_activate_bus = CreateCombobox(ui_buses,ui_buses_count);
    combo_box_deactivate_bus = CreateCombobox(ui_buses, ui_buses_count);
    combo_box_auto_colors = CreateCombobox(ui_auto_colors, ui_auto_colors_count);
    combo_box_auto_destinations = CreateCombobox(ui_auto_destinations, ui_auto_destinations_count);

    //gtk_widget_set_size_request(button, 150,30);
    gtk_widget_set_size_request(darea, TV_WIDTH,TV_HEIGHT );
    gtk_widget_set_size_request(txt_input_command,800,30);

    gtk_container_add (GTK_CONTAINER (window), box1);
    gtk_box_pack_start (GTK_BOX(box1), darea, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box1), box2, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box1), box3, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box1), box4, TRUE, TRUE,0);
    gtk_box_pack_start (GTK_BOX(box1), box5, TRUE, TRUE,0);



    gtk_box_pack_start (GTK_BOX(box2), button, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box2), label_activate_bus, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box2), combo_box_activate_bus, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box2), label_deactivate_bus, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box2), combo_box_deactivate_bus, TRUE, TRUE, 0);

    gtk_box_pack_start (GTK_BOX(box3), button_g_auto, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box3), button_g_ambulance, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box3), button_g_auto_with, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box3), combo_box_auto_colors, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box3), combo_box_auto_destinations, TRUE, TRUE, 0);

    gtk_box_pack_start (GTK_BOX(box4), txt_input_command, TRUE, TRUE, 0);
    gtk_box_pack_start (GTK_BOX(box4), button_execute_command, TRUE, TRUE, 0);

    gtk_box_pack_start (GTK_BOX(box5), label_message, TRUE, TRUE, 0);


    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
    g_signal_connect(G_OBJECT(darea), "expose_event", G_CALLBACK(on_window_expose_event), NULL);
    g_signal_connect(G_OBJECT(window), "configure_event", G_CALLBACK(on_window_configure_event), NULL);
    g_signal_connect(G_OBJECT(button), "clicked",G_CALLBACK (ui_start_simulation_event), NULL);
    g_signal_connect (GTK_COMBO (combo_box_activate_bus)->entry, "activate", G_CALLBACK (ui_activate_bus_event), NULL);
    g_signal_connect (GTK_COMBO (combo_box_deactivate_bus)->entry, "activate", G_CALLBACK (ui_deactivate_bus_event), NULL);
    g_signal_connect(G_OBJECT(button_g_auto), "clicked",G_CALLBACK (ui_generate_auto_event), NULL);
    g_signal_connect(G_OBJECT(button_g_ambulance), "clicked",G_CALLBACK (ui_generate_ambulance_event), NULL);
    g_signal_connect (GTK_COMBO (combo_box_auto_colors)->entry, "activate", G_CALLBACK (ui_auto_colors_event), NULL);
    g_signal_connect (GTK_COMBO (combo_box_auto_destinations)->entry, "activate", G_CALLBACK (ui_auto_destinations_event), NULL);
    g_signal_connect(G_OBJECT(button_g_auto_with), "clicked",G_CALLBACK (ui_generate_auto_with_event), NULL);
    g_signal_connect (txt_input_command, "activate", G_CALLBACK (ui_input_command_event),NULL); 
    g_signal_connect(G_OBJECT(button_execute_command), "clicked",G_CALLBACK (ui_run_command_event), NULL);


    //this must be done before we define our pixmap so that it can reference
    //the colour depth and such
    gtk_widget_show_all(window);

    //set up our pixmap so it is ready for drawing
    pixmap = gdk_pixmap_new(window->window,TV_WIDTH,TV_HEIGHT,-1);
    //because we will be painting our pixmap manually during expose events
    //we can turn off gtk's automatic painting and double buffering routines.
    gtk_widget_set_app_paintable(window, TRUE);
    gtk_widget_set_double_buffered(window, FALSE);

    //(void)g_timeout_add(33, (GSourceFunc)timer_exe, window);
    (void)g_timeout_add(33, (GSourceFunc)timer_exe, darea);

    gtk_main();
    gdk_threads_leave();

    return 0;
}


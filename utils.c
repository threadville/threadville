#include "utils.h"

char * string_alloc(char * s){
    int length = strlen(s) + 1;
    char * new_s = malloc(sizeof(char) * length);
    memcpy(new_s, s, length);
    return new_s;
}


FILE * open_check_file(char * name, char * mode){
    FILE * f = fopen(name, mode);

    if(f == NULL){
        fprintf(stderr, "Error opening file %s\n", name);
        exit(WRNG_FILE_NAME);
    }
    return f;
}

int cmp_intPtr(const void * a, const void * b){
    int _a = *(int*)a;
    int _b = *(int*)b;
    return a-b;
}

int cmp_ints(const int a, const int b){
    return a-b;
}

unsigned int get_random_number(unsigned int min, unsigned int max)
{
    double scaled = (double)rand()/RAND_MAX;

    return (max - min +1)*scaled + min;
}
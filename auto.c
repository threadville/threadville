#include <string.h> // strtok, strcmp
#include <stdlib.h> // atoi

#include "auto.h"

VECTOR_GEN_CODE(tile *, vec_tilePtr)
DICTIONARY_GEN_CODE(char *, tile *, dict_charPtr_tilePtr)
DICTIONARY_GEN_CODE(char *, auto_def, dict_charPtr_autoDef)

dict_charPtr_autoDef * load_autos_db(FILE * db){
    char buffer[BUFF_SIZE];
    char * regex;
    auto_def tmp;

    dict_charPtr_autoDef * dict_def = dict_charPtr_autoDef_class.constructor((int (*)(char*, char*)) strcmp);

    while(fgets(buffer, BUFF_SIZE, db) != NULL){
        regex = strtok(buffer, " ");
        if(strcmp(regex, "auto") == 0){
            char * name = string_alloc(strtok(NULL, " "));
            // parse stops
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.stops = string_alloc(strtok(NULL, ";"));
            // parse stop time
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.tocs_per_stop = atof(strtok(NULL, ";"));
            // parse tales per toc
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.tiles_per_toc = atof(strtok(NULL, ";"));
            // parse loop
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.loop_route = atoi(strtok(NULL, ";"));
            // parse color
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.color[0] = atof(strtok(NULL, ","));
            tmp.color[1] = atof(strtok(NULL, ","));
            tmp.color[2] = atof(strtok(NULL, ","));
            // parse priority
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.priority = atoi(strtok(NULL, "="));
            // parse size
            fgets(buffer, BUFF_SIZE, db);
            strtok(buffer, "=");
            tmp.size = atoi(strtok(NULL, "="));
            // append to dictionary
            dict_def->set(dict_def, name, tmp);
        }
    }
    return dict_def;
}

void generate_random_stops(dict_charPtr_tilePtr * stops_dict, int num_stops, vec_tilePtr * stops){
    int index;
    while(num_stops-- > 0){
        index = rand() % stops_dict->length;
        stops->append(stops, stops_dict->begin[index].value);
    }
}


automobile * automobile_constructor(auto_def def, dict_charPtr_tilePtr * stops_dict, char* manual_stops, int * tics_per_toc, pthread_cond_t * clock_cond, pthread_mutex_t * clock_mtx){
    automobile * self   = malloc(sizeof(automobile));
    *self               = automobile_class;
    self->tics_per_toc  = tics_per_toc;
    self->clock_cond    = clock_cond;
    self->clock_mtx     = clock_mtx;
    self->tocs_per_stop = def.tocs_per_stop;
    self->tiles_per_toc = def.tiles_per_toc;
    self->size          = def.size;
    self->color[0]      = def.color[0];
    self->color[1]      = def.color[1];
    self->color[2]      = def.color[2];
    self->priority      = def.priority;
    self->loop_route    = def.loop_route;
    self->stops         = vec_tilePtr_class.constructor();
    /* def.stops is a c-string (char *, null terminated), every stop name is separeted by ','. This makes a copy of def.stops (string_alloc in stops_str), tokenize stops_str and search that name in stops_dictionary. 
    */
    char * stops_str = manual_stops != NULL ? manual_stops : string_alloc(def.stops);
    char * stop = strtok(stops_str, ",");
    while(stop != NULL){
        if(strcmp(stop, "random") == 0){
            int num_stops = rand() % stops_dict->length;
            generate_random_stops(stops_dict, num_stops, self->stops);
        }
        else if(strcmp(stop, "ambulance") == 0)
            generate_random_stops(stops_dict, 2, self->stops);
        else
            self->stops->append(self->stops, stops_dict->get(stops_dict, stop));
        stop = strtok(NULL, ",");
    }
    free(stops_str);
    self->next_stop = self->stops->begin+1;
    self->route = generate_route(self->stops);
    return self;
}

void * automobile_start(void * s){
    automobile * self = s;
    int offset;
    int wait_time = (*self->tics_per_toc) / self->tiles_per_toc;
    vec_tilePtr_itr next_tile;

    // capture as many tiles on route as size.
    for(int t = 0; t < self->size; ++t)
        while(tile_class.capture(self->route->begin[t], self) == NULL){
            pthread_mutex_lock(self->clock_mtx);
            pthread_cond_wait(self->clock_cond, self->clock_mtx);
            pthread_mutex_unlock(self->clock_mtx);
        }
    self->current_tile = self->route->begin + self->size - 1;

    while(self->alive){
        /* wait for clock tic */
        pthread_mutex_lock(self->clock_mtx);
        pthread_cond_wait(self->clock_cond, self->clock_mtx);
        pthread_mutex_unlock(self->clock_mtx);

        if(wait_time != 0) // gotta wait
            --wait_time;
        else{              // move to next tile
            next_tile = self->current_tile + 1;
            // end of route? 
            if(next_tile == self->route->end) 
                if(self->loop_route){
                    next_tile = self->route->begin;
                    self->next_stop = self->stops->begin+1;
                } else {
                    self->alive = 0; // good bye cruel world. 
                    break;
                }

            // Capture next tile
            if(tile_class.capture(*next_tile, self) != NULL){
                // release previous tile
                offset = (self->current_tile - self->route->begin) - (self->size-1);
                if(offset < 0)
                    tile_class.release(self->route->end[offset]);
                else
                    tile_class.release(self->route->begin[offset]);
                self->current_tile = next_tile;
            } else
                continue;

            // Set time appropiately
            if(*self->current_tile == *self->next_stop){
                self->next_stop++;
                wait_time = self->tocs_per_stop * (*self->tics_per_toc);
            } else{
                wait_time = (*self->tics_per_toc) / self->tiles_per_toc;
                wait_time /= (*self->current_tile)->speed_up;
            }
        }
    }
    // Release all captured tiles
    for(int t = 0; t < self->size; ++t){
        offset = (self->current_tile - self->route->begin) - t;
        if(offset < 0)
            tile_class.release(self->route->end[offset]);
        else
            tile_class.release(self->route->begin[offset]);
    }
    self->dead = 1;
    pthread_exit(NULL);
}

void automobile_kill(automobile * self){
    self->alive = 0;
}

void automobile_destroy(automobile * self){
    self->stops->destroy(self->stops);
    self->route->destroy(self->route);
    free(self);
}

automobile automobile_class = 
        { 0,             // thread id
          NULL,          // route
          NULL,          // stops
          NULL,          // current_tile
          NULL,          // next_stop
          NULL,          // tics_per_toc
          NULL,          // clock_cond
          NULL,          // clock_mtx
          0,             // stop_tics
          0,             // tics_per_tale
          0,             // size
          0,             // loop_route
          0.0, 0.0, 0.0, // color[3]
          1,             // priority
          1,             // alive
          0,             // dead
          automobile_constructor,
          automobile_start,
          automobile_kill,
          automobile_destroy
        };

int cmp_autosPtr(automobile * a1, automobile * a2){
    return a1 - a2;
}

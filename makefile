threadville:threadville.c load_map.c tile.c engine.c auto.c bridge.c utils.c route.c minHeap.c dijkstra.c window.c draw.c
	gcc $^ `pkg-config --cflags --libs cairo gtk+-3.0` -lpthread -o $@
run:
	./threadville conf/threadville.init

valgrind: 
	valgrind --leak-check=yes ./threadville conf/threadville.init
dependencies:
	sudo apt-get install libcairo2-dev
	sudo apt-get install gtk+2.0

clean:
	rm threadville

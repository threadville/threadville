#ifndef H_CAR
#define H_CAR

#include <stdio.h>

#include "gdasc/gdasc_vector.h"
#include "gdasc/gdasc_dictionary.h"
#include "utils.h"
#include "tile.h"

typedef struct {
    char * stops;
    double tocs_per_stop;
    double tiles_per_toc; 
    int loop_route;
    double color[3];
    int priority;
    int size;
} auto_def;

/* Containers declarations */
VECTOR_DECLARE(tile *, vec_tilePtr)
DICTIONARY_DECLARE(char *, tile *, dict_charPtr_tilePtr)
DICTIONARY_DECLARE(char *, auto_def, dict_charPtr_autoDef)

typedef struct automobile automobile;
struct automobile {
    pthread_t tid;
    vec_tilePtr * route;
    vec_tilePtr * stops;
    vec_tilePtr_itr current_tile;
    vec_tilePtr_itr next_stop;
    int * tics_per_toc;
    pthread_cond_t * clock_cond;
    pthread_mutex_t * clock_mtx;
    double tocs_per_stop;
    double tiles_per_toc;
    int size;
    int loop_route;
    double color[3];
    int priority;
    int alive;
    int dead;
    automobile * (*constructor) (auto_def, dict_charPtr_tilePtr *, char*, int *, pthread_cond_t *, pthread_mutex_t *);
    void * (*start) (void *);
    void (*kill_signal) (automobile *);
    void (*destroy) (automobile *);
};
extern automobile automobile_class;

/* Prototypes */
dict_charPtr_autoDef * load_autos_db(FILE * db);
vec_tilePtr * generate_route(vec_tilePtr * stops);
int cmp_autosPtr(automobile * a1, automobile * a2);
void generate_random_stops(dict_charPtr_tilePtr * stops_dict, int num_stops, vec_tilePtr * stops);
#endif

#include <gtk/gtk.h>
#include <math.h>
#include "draw.h"

void draw_arrow(cairo_t *cr, int x, int y, int rotation, int offset)
{
    cairo_save(cr);
    cairo_set_source_rgb(cr, 0.5, 0.5, 0);
    cairo_set_line_width(cr, 1);
    if(rotation == 180)
    {
        cairo_translate(cr,x + offset/2,y + offset);
    }

    else if(rotation == 0){
        cairo_translate(cr,x + offset/2,y);
    }
    else if(rotation == 90){
        cairo_translate(cr,x + offset ,y - offset/2);
    }
    else if(rotation == 270){
        cairo_translate(cr,x ,y);
    }


    cairo_rotate(cr, rotation * M_PI / 180.0f);
    double factor = 1.0f;
    for (int i = 0; i < 8; i++) {
        cairo_line_to(cr, factor * points[i][0], factor * points[i][1]);
    }
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);

    cairo_restore(cr);
}


void draw_triangle(cairo_t *cr)
{
    cairo_set_source_rgb(cr, 1, 0, 0);
    cairo_set_line_width(cr, 1);
    double factor = 1.0f;
    for (int i = 0; i < 4; i++) {
        cairo_line_to(cr, factor * triangle[i][0], factor * triangle[i][1]);
    }
    cairo_close_path(cr);
    cairo_stroke_preserve(cr);
    cairo_fill(cr);
}

void draw_dash(cairo_t *cr, double x1, double y1, double x2, double y2, double width, double space, double r, double g, double b)
{
    static double dashed3[1];
    dashed3[0] = space;
    cairo_save(cr);
    cairo_set_line_width(cr, width);
    cairo_set_source_rgb(cr, r, g, b);
    cairo_set_dash(cr, dashed3, 1, 0);
    cairo_move_to(cr, x1, y1);
    cairo_line_to(cr, x2, y2);
    cairo_stroke(cr);
    cairo_restore(cr);
}

void draw_tile(tile *tile, cairo_t *cr, int tile_length, int x, int y, double r, double g, double b) {
    int left = 0, down = 0, right= 0, up = 0, total = 0, rotation = 0, row=0, column = 0;
    cairo_set_source_rgb(cr, r,g,b);
    cairo_rectangle(cr, x, y, tile_length, tile_length);
    cairo_fill(cr);

    if(tile->links[0] != NULL) left = 1;
    if(tile->links[1] != NULL) down = 1;
    if(tile->links[2] != NULL) right = 1;
    if(tile->links[3] != NULL) up = 1;
    total = left + down + right + up;
    if(total == 1)
    {
        row = (tile->x - 10) % 8;
        if(tile->y == 4 || tile->y == 11 || (tile->y == 16 && tile->speed_up == 1) || tile->y == 21 || tile->y == 28)
            column =  1;
        if(row == 0 || column == 1) {
            if (left) rotation = 180;
            if (down) rotation = 90;
            if (up) rotation = 270;
            if (right) rotation = 0;
            draw_arrow(cr, x, y, rotation, tile_length);
        }
        if(up == 1 && tile->x != 7)
            draw_dash(cr,x,y,x, y+ tile_length,1.0, 5.0, 0.5,0.5,0);
        if(left = 1 && tile->y == 15 && tile->speed_up == 2){
            draw_dash(cr,x,y+tile_length-1,x+tile_length, y+ tile_length-1,2.0,2.0, 0.5,0.5,0);
            draw_dash(cr,x,y,x+tile_length, y,1.0,10.0, 1.0,1.0,1.0);
        }
        if(right = 1 && tile->y == 16 && tile->speed_up == 2){
            draw_dash(cr,x,y+1,x+tile_length, y+1,2.0,2.0, 0.5,0.5,0);
            draw_dash(cr,x,y+tile_length,x+tile_length, y+tile_length,1.0,10.0, 1.0,1.0,1.0);
        }


    }

}
#include <stdio.h>
#include <ctype.h>
#include "threadville.h"

/* Containers code */
VECTOR_GEN_CODE(request, vec_request)
VECTOR_GEN_CODE(repair, vec_repair)
VECTOR_GEN_CODE(bridge *, vec_bridgePtr);

int main(int argc, char * argv[]){

    if(argc != 2){
        fprintf(stderr, "Wrong number of parameters.\n");
        fprintf(stderr, "Usage: $ %s init_file.conf\n", argv[0]);
        exit(NO_CONF_FILE);
    }

    pthread_t gui;
    struct init_args main_args = {argc, argv};

    FILE * init_file  = open_check_file(argv[1], "r");
    engine_init(init_file);
    fclose(init_file);

    pthread_create(&gui, NULL, window_load, &main_args);
    engine_main_loop();
    engine_end();

    pthread_join(gui, NULL);
    return 0;
}

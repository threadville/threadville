#ifndef DRAW_H
#define DRAW_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tile.h"
#include "auto.h"

/* Prototypes */
static int points[8][2] = {
        { 0, 7 },
        { 5, 7 },
        { 5, 6 },
        { 7, 8 },
        { 5, 10 },
        { 5, 9 },
        { 0, 9 },
        { 0, 7 }
};

static int triangle[4][2] = {
        { 2, 5 },
        { 4, 3 },
        { 6, 5 },
        { 2, 5 }
};

void draw_tile(tile *tile, cairo_t *cr, int tile_length,int x, int y, double r, double g, double b);
void draw_arrow(cairo_t *cr, int x, int y, int rotation,int offset);
void draw_triangle(cairo_t *cr);

#endif

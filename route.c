#include "auto.h"
//#include "threadville.h"
#include "dijkstra.h"

void shortest_route(tile* source, tile* destination, vec_tilePtr * route);
void print_route(tile* source, tile* destination, vec_tilePtr * route);


vec_tilePtr * generate_route(vec_tilePtr * stops){

    vec_tilePtr * route = vec_tilePtr_class.constructor();

    for(int i=stops->length-1;i >= 1 ;i--)
    {
        tile* source = stops->begin[i-1];
        tile* destination = stops->begin[i];
        shortest_route(source, destination, route);
    }

//    print_route(stops->begin[0], stops->begin[stops->length-1], route);
    return route;
}

void shortest_route(tile* source, tile* destination, vec_tilePtr * route){
    dijkstra(source, destination, route);
}

void print_route(tile* source, tile* destination, vec_tilePtr * route){
    printf("source {%d,%d} destination {%d,%d}\n", source->x,source->y,
            destination->x, destination->y);
    for(int i=0;i < route->length;i++) {
        if (route->begin[i]->name != NULL)
            printf("\n%s", route->begin[i]->name);
        printf("-> {%d,%d}", route->begin[i]->x, route->begin[i]->y);
    }
    printf("\n");
}

#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFF_SIZE 1024
#define WRNG_FILE_NAME 6001

/* Prototypes */
FILE * open_check_file(char *, char * mode);
char * string_alloc(char * s);
int cmp_intPtr(const void *, const void *); // for qsort
int cmp_ints(const int, const int);         // for searching in a vector of ints
unsigned int get_random_number(unsigned int min, unsigned int max); //Generates a random number within a range
#endif
